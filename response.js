function success(req, res, result, status) {
  let statusCode = status || 200;

  res.status(statusCode).send({
    error: false,
    msg: "OK",
    total: 1,
    pagination: {
      total: 1,
      per_page: 1,
      current_page: 1,
      last_page: 1,
      from: 1,
      to: 2,
    },
    data: result,
  });
}

function error(req, res, message, status) {
  res.status(status).send({
    error: true,
    msg: message,
    total: 0,
    pagination: {
      total: 0,
      per_page: 0,
      current_page: 0,
      last_page: 0,
      from: 0,
      to: 0,
    },
    data: {},
  });
}

module.exports = {
  success,
  error,
};
