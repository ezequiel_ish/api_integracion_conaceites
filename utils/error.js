function handleError(message, code) {
  let nError = new Error(message);

  if (code) {
    nError.statusCode = code;
  }
  return nError;
}

module.exports = handleError;
