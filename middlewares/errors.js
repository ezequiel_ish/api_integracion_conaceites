const { error } = require("../response");

function errors(err, req, res, next) {
  const message = err.message || "Error interno";
  const status = err.statusCode || 500;

  error(req, res, message, status);
}

module.exports = errors;
