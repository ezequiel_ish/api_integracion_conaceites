const express = require("express");
require("dotenv").config();
const bodyParser = require("body-parser");
const app = express();
const routes = require("./routes");
const errors = require("./middlewares/errors");
const handleError = require("./utils/error");
function initServer() {
  const nodo = app.listen(process.env.PORT || 5000, () => {
    console.log(`node running on port: ${nodo.address().port}`);
  });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
routes(app);

app.use((req, resp, next) => {
  throw handleError("Ruta no encontrada", 404);
});
app.use(errors);

initServer();
