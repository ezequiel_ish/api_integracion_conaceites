const clientsJson = require("../data/clients.json");

function getClient(nit) {
  let client = clientsJson
    .filter((client) => client["nitcedula"] == nit)
    .shift();
  client &&
    (client = {
      "#esta": client["nombreestablecimiento"],
      "#cliente": client["nombrecliente"],
      "#dire": client["direccion"],
      "#barr": client["barrio"],
      "#cuidad": client["ciudad"],
      "#tipo": client["tipodepago"],
      "#cupo": client["cupodisponible"],
      "#limite": client["limitedecredito"],
      "#grupo": client["grupodelcliente"],
      "#condi": client["condicionesdepago"],
    });

  return client || {};
}

module.exports = {
  getClient,
};
