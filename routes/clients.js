const express = require("express");
const router = express.Router();
const Services = require("../services/clients");
const { success } = require("../response");
const handleError = require("../utils/error");

router.post("/", async function (req, res, next) {
  try {
    const nit = req.body.nit;
    const result = Services.getClient(nit);
    if (Object.keys(result).length === 0) {
      throw handleError("No se encontraron coincidencias", 404);
    }

    success(req, res, result, 200);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
