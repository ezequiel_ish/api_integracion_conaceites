const clients = require("./clients");

function routes(serverExpress) {
  serverExpress.use("/api/clients", clients);
}

module.exports = routes;
